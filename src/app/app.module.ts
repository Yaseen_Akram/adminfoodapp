// Importing the Requiresd Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';
import { ReactiveFormsModule} from '@angular/forms';
import { FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { ToastrModule} from 'ngx-toastr';
import {DataTablesModule} from 'angular-datatables';

// importing the Required Components
import { AppComponent } from './app.component';
import { PreloaderComponent } from './components/layouts/preloader/preloader.component';
import { SideNavbarComponent } from './components/layouts/side-navbar/side-navbar.component';
import { TopNavbarComponent } from './components/layouts/top-navbar/top-navbar.component';
import { QuickBarComponent } from './components/layouts/quick-bar/quick-bar.component';

// importing the Required the Services
import { ConstantserviceService } from './constantservice.service';
import { ServiceService } from './service.service';
import { AuthService } from './Pages/login-module/auth.service';


@NgModule({
  declarations: [
    AppComponent,
    PreloaderComponent,
    SideNavbarComponent,
    TopNavbarComponent,
    QuickBarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ToastrModule.forRoot(),
    DataTablesModule
  ],
  providers: [ConstantserviceService, ServiceService, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
