import { Injectable } from '@angular/core';
import {ConstantserviceService} from '../app/constantservice.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  constructor(
    private constservice: ConstantserviceService,
    private httpclient: HttpClient) { }


getWithoutToken(apiName) {
  return this.httpclient.get(this.constservice.API_ENDPOINT + apiName);
}

 // makes a http get call to server
 public get(apiName: string) {
  console.log('Control is comming into' + apiName );
  const token = localStorage.getItem('token');
  const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: token
  });
  return this.httpclient.get(this.constservice.API_ENDPOINT + apiName, {
     headers
  });
}
// makes a http get call to server
public getOne(apiName: string, id: string) {
  const token = localStorage.getItem('token');
  const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
  });
  return this.httpclient.get(this.constservice.API_ENDPOINT + apiName + '/' + id, {
     headers
  });
}

// saves data on server
public post(apiName: string, data: any) {
  console.log('hiting post' + apiName)
  const token = localStorage.getItem('token');
  const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
  });
  return this.httpclient.post(this.constservice.API_ENDPOINT + apiName, data, {
     headers
  });
}

// updates data on server
public put(apiName: string, id: string, data: any) {
  const token = localStorage.getItem('token');
  const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
  });
  return this.httpclient.put(this.constservice.API_ENDPOINT + apiName + '/' + id, data, {
       headers
  });
}

// updates data on server
public patch(apiName: string, id: string, data: any) {
  const token = localStorage.getItem('token');
  const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
  });
  return this.httpclient.patch(this.constservice.API_ENDPOINT + apiName + '/' + id, data, {
       headers
  });
}
// deletes data from server
public delete(apiName: string, id: string) {
  console.log('in the Main Service ' + apiName, id)
  const token = localStorage.getItem('token');
  const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': token
  });
  return this.httpclient.delete(this.constservice.API_ENDPOINT + apiName + '/' + id, {
       headers
  });
}

public UploadImage(apiName: string, data: any) {
  const token = localStorage.getItem('token');
  const headers = new HttpHeaders({
      'Authorization': token,
      'X-Requested-With': 'HttpClient'
  });
  return this.httpclient.post(this.constservice.API_ENDPOINT + apiName, data, {
      headers
  });
}

}