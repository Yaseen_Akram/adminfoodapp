import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './Pages/dashboard/dashboard.component';
import { LoginModuleModule } from './Pages/login-module/login-module.module';

const routes: Routes = [
{ path: '', redirectTo: 'login', pathMatch: 'full' },
{ path: 'login', loadChildren: () => import('./Pages/login-module/login-module.module').then(m => m.LoginModuleModule) },

{path: 'dashboard', component: DashboardComponent},

{ path: 'banners', loadChildren: () => import('./Pages/banners/banners.module').then(m => m.BannersModule) },

{ path: 'restaurants', loadChildren: () => import('./Pages/restaurants/restaurants.module').then(m => m.RestaurantsModule) },

{ path: 'cuisines', loadChildren: () => import('./Pages/cuisines/cuisines.module').then(m => m.CuisinesModule) },

{ path: 'orders', loadChildren: () => import('./Pages/orders/orders.module').then(m => m.OrdersModule) },

{ path: 'pricing', loadChildren: () => import('./Pages/pricing/pricing.module').then(m => m.PricingModule) },

{ path: 'tags', loadChildren: () => import('./Pages/tags/tags.module').then(m => m.TagsModule) },

{ path: 'brands', loadChildren: () => import('./Pages/brands/brands.module').then(m => m.BrandsModule) },

{ path: 'topCategories', loadChildren: () => import('./Pages/top-categories/top-categories.module').then(m => m.TopCategoriesModule) },

{ path: 'users', loadChildren: () => import('./Pages/users/users.module').then(m => m.UsersModule) },

{ path: 'deliveryboy', loadChildren: () => import('./Pages/delivery-boy/delivery-boy.module').then(m => m.DeliveryBoyModule) }




];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
