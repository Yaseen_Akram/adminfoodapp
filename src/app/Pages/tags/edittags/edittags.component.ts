import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TagsServiceService } from '../tags-service.service';

@Component({
  selector: 'app-edittags',
  templateUrl: './edittags.component.html',
  styleUrls: ['./edittags.component.css']
})
export class EdittagsComponent implements OnInit {
  Tags: FormGroup;
  tagsData: any;
  tagsSingleData: any;
  singleTagId: any;
  constructor(
    private route: Router,
    private toaster: ToastrService,
    private tagsService: TagsServiceService,
    private activeroute: ActivatedRoute
    ) { }

  ngOnInit(): void {
  this.buildFormEdittag();
  this.activeroute.params.subscribe(
    (data) => {
        console.log(JSON.stringify(data.id));
        this.singleTagId = data.id;
    });

  this.tagsService.getTagsbyId(this.singleTagId)
     .subscribe((res) => {
      this.tagsSingleData = res;
      console.log('Tags data' + JSON.stringify(this.tagsSingleData));
      this.updateValue(this.tagsSingleData);
    });

  }


  updateValue(tagData){
    this.Tags.patchValue({
      tag: tagData.tag
    });

  }
  buildFormEdittag(){
  this.Tags = new FormGroup({
    tag: new FormControl(null, [Validators.required])
    });
  }

  onUpdateTags(){
    console.log(this.Tags.value);
    this.tagsData = this.Tags.value;
    this.tagsService.editTags(this.singleTagId, this.tagsData)
    .subscribe((response) => {
      console.log('this respose data' + JSON.stringify(response));
    });
    this.toaster.success('Tags Edited SuccessFully !');
    this.route.navigate(['/tags/hometag']);
  }
}
