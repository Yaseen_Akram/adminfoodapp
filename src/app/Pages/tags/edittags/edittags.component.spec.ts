import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EdittagsComponent } from './edittags.component';

describe('EdittagsComponent', () => {
  let component: EdittagsComponent;
  let fixture: ComponentFixture<EdittagsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EdittagsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EdittagsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
