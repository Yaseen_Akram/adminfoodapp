import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TagsComponent } from './tags.component';
import { HometagComponent } from './hometag/hometag.component';
import { CreatetagsComponent } from './createtags/createtags.component';
import { EdittagsComponent } from './edittags/edittags.component';


const routes: Routes = [
  { path: '', component: TagsComponent },
  { path: 'hometag', component: HometagComponent},
  { path: 'create', component: CreatetagsComponent},
  { path: 'edit/:id', component: EdittagsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TagsRoutingModule { }
