import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HometagComponent } from './hometag.component';

describe('HometagComponent', () => {
  let component: HometagComponent;
  let fixture: ComponentFixture<HometagComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HometagComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HometagComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
