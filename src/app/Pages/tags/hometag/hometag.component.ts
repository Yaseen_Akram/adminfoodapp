import { Component, OnInit } from '@angular/core';
import {TagsServiceService} from '../tags-service.service'; 
import { Router } from '@angular/router';


@Component({
  selector: 'app-hometag',
  templateUrl: './hometag.component.html',
  styleUrls: ['./hometag.component.css']
})
export class HometagComponent implements OnInit {
  TagsList: any;
  constructor(
    private tagservice: TagsServiceService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.getTagsList();
  }

  getTagsList(){
    this.tagservice.getAlltags()
      .subscribe((response) => {
        this.TagsList = response;
        console.log(' This Tag List ' + JSON.stringify(this.TagsList));
      });
  }
  OnAddTags(){
    this.route.navigate(['/tags/create']);
  }

  onEditTags(id) {
    this.route.navigate(['/tags/edit/' + id]);
  }

  onDeleteTags(id) {
    this.tagservice.deleteTags(id)
    .subscribe((response) => {
        console.log(JSON.stringify(response));
    });
  }
}