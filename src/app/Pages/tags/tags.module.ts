import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TagsRoutingModule } from './tags-routing.module';
import { TagsComponent } from './tags.component';
import { HometagComponent } from './hometag/hometag.component';
import { CreatetagsComponent } from './createtags/createtags.component';
import { EdittagsComponent } from './edittags/edittags.component';



@NgModule({
  declarations: [TagsComponent, HometagComponent, CreatetagsComponent, EdittagsComponent],
  imports: [
    CommonModule,
    TagsRoutingModule,
    ReactiveFormsModule
  ]
})
export class TagsModule { }
