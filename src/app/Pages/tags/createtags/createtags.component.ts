import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup} from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TagsServiceService } from '../tags-service.service';

@Component({
  selector: 'app-createtags',
  templateUrl: './createtags.component.html',
  styleUrls: ['./createtags.component.css']
})
export class CreatetagsComponent implements OnInit {
  Tags: FormGroup;
  tagsData: any;
  constructor(
    private route: Router,
    private toaster: ToastrService,
    private tagsService: TagsServiceService
    ) { }

  ngOnInit(): void {
  this.buildFormCuisine();
  }

  buildFormCuisine(){
  this.Tags = new FormGroup({
    tag: new FormControl(null, [Validators.required])
    });
  }

  onSaveTags(){
    console.log(this.Tags.value);
    this.tagsData = this.Tags.value;
    this.tagsService.addTags(this.tagsData)
    .subscribe((response) => {
      console.log('this respose data' + JSON.stringify(response));
    });
    this.toaster.success('Tags Added SuccessFully !');
    this.route.navigate(['/tags/hometag']);
  }
}
