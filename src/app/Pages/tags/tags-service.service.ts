import { Injectable } from '@angular/core';
import {ServiceService} from '../../service.service';

@Injectable({
  providedIn: 'root'
})
export class TagsServiceService {

  constructor(
    private service: ServiceService
  ) { }

  getAlltags(){
    return this.service.get('tags/all');
  }

  getTagsbyId(id){
    return this.service.getOne('tags', id);
  }
  addTags(data){
    return this.service.post('tags', data);
  }


  editTags(id, data){
    return this.service.put('tags', id, data);
  }

  deleteTags(id){
    return this.service.delete('tags', id);
  }

}

