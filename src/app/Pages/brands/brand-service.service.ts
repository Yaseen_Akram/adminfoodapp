import { Injectable } from '@angular/core';
import {ServiceService} from '../../service.service';


@Injectable({
  providedIn: 'root'
})
export class BrandServiceService {

  constructor(
    private service: ServiceService
  ) { }

  getAllBrands(){
    return this.service.get('topBrands/list');
  }

  public uploadImage(file) {
    const formData = new FormData();
    formData.append('file', file);

    return this.service.UploadImage('uploads', formData);
  }

  addBrands(data){
    return this.service.post('topBrands/create', data);
  }

  editBrands( id, data){
    return this.service.put('topBrands/update', id , data);
  }

  deleteBrands(id){
    return this.service.delete('topBrands', id);
  }
}
