import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { BrandsRoutingModule } from './brands-routing.module';
import { BrandsComponent } from './brands.component';
import { HomebrandComponent } from './homebrand/homebrand.component';
import { CreatebrandsComponent } from './createbrands/createbrands.component';
import { EditbrandsComponent } from './editbrands/editbrands.component';



@NgModule({
  declarations: [BrandsComponent, HomebrandComponent, CreatebrandsComponent, EditbrandsComponent],
  imports: [
    CommonModule,
    BrandsRoutingModule,
    ReactiveFormsModule
  ]
})
export class BrandsModule { }
