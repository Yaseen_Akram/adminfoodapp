import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatebrandsComponent } from './createbrands.component';

describe('CreatebrandsComponent', () => {
  let component: CreatebrandsComponent;
  let fixture: ComponentFixture<CreatebrandsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatebrandsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatebrandsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
