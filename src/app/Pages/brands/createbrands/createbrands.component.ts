import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { BrandServiceService } from '../brand-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-createbrands',
  templateUrl: './createbrands.component.html',
  styleUrls: ['./createbrands.component.css']
})
export class CreatebrandsComponent implements OnInit {
brands: FormGroup;
url: any;
ImageUrlid: any;
imageSelected: boolean;
enablesaveButton = false;
  constructor(
    private brandService: BrandServiceService,
    private toaster: ToastrService,
    private route: Router
    ) { }

  ngOnInit(): void {
  this.buildForm();
  }

  buildForm(){
    this.brands = new FormGroup({
      brandName : new FormControl(null, [Validators.required]),
      imageURL : new FormControl (null, [Validators.required])
    });

  }

  onSaveBrand(){
    this.brands.setValue({
      brandName: this.brands.value.brandName,
      imageURL: this.ImageUrlid
    });
    console.log(this.brands.value);
    this.brandService.addBrands(this.brands.value)
    .subscribe((response) => {
        console.log(JSON.stringify(response));
        this.route.navigate(['/brands/homebrand']);
        this.toaster.success('Brand is Added Successfull !!')
    });
    }

  readUrl(event) { // invoke when user select image to upload
    if (event.target.files && event.target.files[0]) {
        const reader = new FileReader();
        reader.onload = (event1: any) => {
            this.url = event1.target.result;
            this.imageSelected = true;
        };
        reader.readAsDataURL(event.target.files[0]);

        this.saveImageOnServer(event.target.files[0]);
    }

  }

  saveImageOnServer(image) {
    this.brandService.uploadImage(image)
        .subscribe((res: any) => {
          console.log(res._id);
          this.ImageUrlid = res._id;
        });
    this.toaster.success('Image is uploaded Successfull !!');
}
}
