import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BrandsComponent } from './brands.component';
import { HomebrandComponent } from './homebrand/homebrand.component';
import { CreatebrandsComponent } from './createbrands/createbrands.component';
import { EditbrandsComponent } from './editbrands/editbrands.component';

const routes: Routes = [
  { path: '', component: BrandsComponent },
  { path: 'homebrand', component: HomebrandComponent},
  { path: 'create', component: CreatebrandsComponent},
  { path: 'edit/:id', component: EditbrandsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BrandsRoutingModule { }
