import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomebrandComponent } from './homebrand.component';

describe('HomebrandComponent', () => {
  let component: HomebrandComponent;
  let fixture: ComponentFixture<HomebrandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomebrandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomebrandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
