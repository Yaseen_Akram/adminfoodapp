import { Component, OnInit } from '@angular/core';
import { BrandServiceService } from '../brand-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homebrand',
  templateUrl: './homebrand.component.html',
  styleUrls: ['./homebrand.component.css']
})
export class HomebrandComponent implements OnInit {
BrandList: any;
BrandData: any;
  constructor(
    private brandService: BrandServiceService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.getBrands();
  }

  getBrands(){
    this.brandService.getAllBrands()
      .subscribe((respone) => {
          this.BrandData = respone;
          this.BrandList = this.BrandData.data;
          console.log('This getBrands ' + JSON.stringify(this.BrandList));
      });
  }
  OnAddBrands(){
    this.route.navigate(['/brands/create']);
  }

  onEditBrands(id){
      this.route.navigate(['/brands/edit/' + id]);
  }

  onDeleteBrands(id){
      this.brandService.deleteBrands(id)
      .subscribe((res) => {
          console.log(JSON.stringify(res) );
      });
      this.getBrands();
    }
}
