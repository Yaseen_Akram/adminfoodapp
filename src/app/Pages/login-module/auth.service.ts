import { Injectable } from '@angular/core';
import {ConstantserviceService } from '../../constantservice.service';
import { HttpClient, HttpHeaders} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private httpclient: HttpClient,
    private constantservice: ConstantserviceService) { }

    login(data){
      const headers = new HttpHeaders({
        'Content-Type': 'application/json'
      });
      return this.httpclient.post(this.constantservice.LOGIN_AUTH + 'auth/local' , data ,
      { headers });

    }

    getUserData() {
      const token = localStorage.getItem('token');
      const headers = new HttpHeaders({
          'Content-Type': 'application/json',
           Authorization : token
      });
      return this.httpclient.get(this.constantservice.API_ENDPOINT + 'users/me', {
        headers
      });
  }

  }
