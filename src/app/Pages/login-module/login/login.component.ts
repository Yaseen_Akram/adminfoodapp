import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { ToastrService } from 'ngx-toastr';
import { CookieService} from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  // isVisibleSidebar = false;
  isRememberMe = false;
  public isLoading = false;
  public loginImage;

  constructor(
    private authService: AuthService,
    private toaster: ToastrService,
    private cookies: CookieService,
    private router: Router

  ) {
    if (localStorage.getItem('logo')) {
      const logo: any = JSON.parse(localStorage.getItem('logo'));
      this.loginImage = logo.adminLogoUrl ? logo.adminLogoUrl : 'assets/img/logo.jpeg';
    } else {
      this.loginImage = 'assets/img/logo.jpeg';
    }
   }

  ngOnInit(): void {
    this.buildLoginForm();
    this.getCookieData();
  }
  private buildLoginForm() {
    this.loginForm = new FormGroup({
      email: new FormControl(null, [ Validators.required, Validators.email]),
      password: new FormControl(null, [Validators.required])
    });
  }

  // gets cookie data
  private getCookieData() {
    const user: any = this.cookies.get('user');
    if (user !== undefined && user !== null) {
      this.loginForm.get('email').setValue(user.email);
      this.loginForm.get('password').setValue(user.password);
    }
  }

  // sends user credentials to server
  login() {
    if (this.isRememberMe) {
      this.cookies.set('user', this.loginForm.value);
    }
    this.isLoading = true;
    console.log( 'logion data', this.loginForm.value);
    this.authService.login(this.loginForm.value).subscribe((res: any) => {
      localStorage.setItem('token', 'Bearer ' + res.token);
      this.authService.getUserData().subscribe((user: any) => {
        if (user.role !== 'admin'){
             return this.toaster.error('You are not authorized to login', 'Un-Authorized', { timeOut: 3000 });
        }
        localStorage.setItem('id', user._id);
        localStorage.setItem('role', user.role);
        localStorage.setItem('isVisibleSidebar', 'true' );
        this.toaster.success('Login successful', 'Success', { timeOut: 3000 });
        this.isLoading = false;
        this.router.navigate(['/dashboard']);
      }, error => {
        this.isLoading = false;
        this.toaster.error(error.error.message, 'Error', { timeOut: 3000 })
      });
    }, error => {
      this.isLoading = false;
      this.toaster.error(error.error.message, 'Error', { timeOut: 3000 });
    });
  }
}
