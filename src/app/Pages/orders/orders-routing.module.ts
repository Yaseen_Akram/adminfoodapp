import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OrdersComponent } from './orders.component';
import { HomeorderComponent } from './homeorder/homeorder.component';

const routes: Routes = [
  { path: '', component: OrdersComponent },
  { path: 'homeorder', component : HomeorderComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OrdersRoutingModule { }
