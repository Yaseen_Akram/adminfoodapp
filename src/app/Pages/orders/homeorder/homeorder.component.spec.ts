import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeorderComponent } from './homeorder.component';

describe('HomeorderComponent', () => {
  let component: HomeorderComponent;
  let fixture: ComponentFixture<HomeorderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeorderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeorderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
