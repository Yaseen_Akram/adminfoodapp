import { Component, OnInit } from '@angular/core';
import {OrderServiceService} from '../order-service.service';

@Component({
  selector: 'app-homeorder',
  templateUrl: './homeorder.component.html',
  styleUrls: ['./homeorder.component.css']
})
export class HomeorderComponent implements OnInit {
orderList: any;
  constructor(
    private orderservice: OrderServiceService
  ) { }

  ngOnInit(): void {
    this.getAllordersList()
  }

  getAllordersList(){
    this.orderservice.getAllOrders()
      .subscribe((response) => {
          this.orderList = response;
          console.log('This Order List' + JSON.stringify(this.orderList));
      });
  }
}
