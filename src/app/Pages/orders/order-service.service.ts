import { Injectable } from '@angular/core';
import { ServiceService} from '../../service.service';

@Injectable({
  providedIn: 'root'
})
export class OrderServiceService {

  constructor(
    private service: ServiceService
  ) { }

  getAllOrders(){
    return this.service.get('orders/all/order');
  }
}
