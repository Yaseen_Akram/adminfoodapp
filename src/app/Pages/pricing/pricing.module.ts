import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { PricingRoutingModule } from './pricing-routing.module';
import { PricingComponent } from './pricing.component';
import { HomepricingComponent } from './homepricing/homepricing.component';
import { CreatepricingComponent } from './createpricing/createpricing.component';
import { EditpricingComponent } from './editpricing/editpricing.component';




@NgModule({
  declarations: [PricingComponent, HomepricingComponent, CreatepricingComponent, EditpricingComponent],
  imports: [
    CommonModule,
    PricingRoutingModule,
    ReactiveFormsModule
  ]
})
export class PricingModule { }
