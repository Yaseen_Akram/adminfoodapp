import { Component, OnInit } from '@angular/core';
import { PricingServiceService } from '../pricing-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homepricing',
  templateUrl: './homepricing.component.html',
  styleUrls: ['./homepricing.component.css']
})
export class HomepricingComponent implements OnInit {
  PricingList: any;
  constructor(
    private priceservice: PricingServiceService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.getPricing();
  }

  getPricing(){
    this.priceservice.getPricing()
      .subscribe( (response) => {
          this.PricingList = response;
          console.log('The Pricing List Are ' + JSON.stringify(this.PricingList));
      });
  }

  OnAddPricing(){
    this.route.navigate(['/pricing/create']);
  }

  onEditPricing(id){
    this.route.navigate(['/pricing/edit/' + id]);
  }

  onDeletePricing(id){
    console.log(id);
    this.priceservice.deletePricing(id);
  }
}
