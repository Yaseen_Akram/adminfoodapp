import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatepricingComponent } from './createpricing.component';

describe('CreatepricingComponent', () => {
  let component: CreatepricingComponent;
  let fixture: ComponentFixture<CreatepricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatepricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatepricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
