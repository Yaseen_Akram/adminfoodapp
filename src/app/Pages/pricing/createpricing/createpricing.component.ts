import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators} from '@angular/forms';
import { Router } from '@angular/router';
import { PricingServiceService } from '../pricing-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-createpricing',
  templateUrl: './createpricing.component.html',
  styleUrls: ['./createpricing.component.css']
})
export class CreatepricingComponent implements OnInit {
  princingForm: FormGroup;
  pricingData: any;
  constructor(
    private route: Router,
    private pricingservice: PricingServiceService,
    private toaster: ToastrService
  ) { }

  ngOnInit(): void {
    this.buildPricingForm();
  }

  buildPricingForm(){
    this.princingForm = new FormGroup({
      promotionName : new FormControl(null, [Validators.required]),
      promotionDescription: new FormControl(null, [Validators.required]),
      pricePerDay: new FormControl(null, [Validators.required]),
      pricePerMonth: new FormControl(null, [Validators.required]),
    });

  }
  onSavePricing(){
    this.pricingData = this.princingForm.value;
    this.pricingservice.addPricing(this.pricingData)
    .subscribe((response) =>{
        console.log('this is the saving price ' + JSON.stringify(response));
        this.toaster.success('Pricing is Added Successfully !');
        this.route.navigate(['pricing/homepricing']);
    });
  }
}
