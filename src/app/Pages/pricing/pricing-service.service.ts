import { Injectable } from '@angular/core';
import {ServiceService} from '../../service.service';
@Injectable({
  providedIn: 'root'
})
export class PricingServiceService {

  constructor(
    private service: ServiceService
  ) { }

  getPricing(){
    return this.service.get('priceService');
  }

  getpricingbyId(id){
    return this.service.getOne('priceService', id);
  }
  addPricing(data){
    return this.service.post('priceService', data);
  }

  editPricing(id, data){
    return this.service.put('priceService/priceUpdate', id, data);
  }

  deletePricing(id){
    console.log('control is comming into service deleting');
    return this.service.delete('priceService', id);
  }
}
