import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PricingComponent } from './pricing.component';
import { HomepricingComponent } from './homepricing/homepricing.component';
import { CreatepricingComponent } from './createpricing/createpricing.component';
import { EditpricingComponent } from './editpricing/editpricing.component';


const routes: Routes = [
  { path: '', component: PricingComponent },
  { path: 'homepricing', component: HomepricingComponent },
  { path: 'create', component: CreatepricingComponent },
  { path: 'edit/:id', component: EditpricingComponent }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PricingRoutingModule { }
