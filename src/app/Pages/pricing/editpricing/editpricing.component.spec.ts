import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditpricingComponent } from './editpricing.component';

describe('EditpricingComponent', () => {
  let component: EditpricingComponent;
  let fixture: ComponentFixture<EditpricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditpricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditpricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
