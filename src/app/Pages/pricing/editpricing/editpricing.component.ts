import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { PricingServiceService } from '../pricing-service.service';
import { ToastrService } from 'ngx-toastr';
 

@Component({
  selector: 'app-editpricing',
  templateUrl: './editpricing.component.html',
  styleUrls: ['./editpricing.component.css']
})
export class EditpricingComponent implements OnInit {
  editprincingForm: FormGroup;
  pricingData: any;
  pricingSingleData: any;
  singlePriceId: any;
  constructor(
    private route: Router,
    private pricingservice: PricingServiceService,
    private toaster: ToastrService,
    private activeroute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.buildPricingForm();
    this.activeroute.params.subscribe(
      (data) => {
          console.log(JSON.stringify(data.id));
          this.singlePriceId = data.id;
      });
    this.pricingservice.getpricingbyId(this.singlePriceId)
      .subscribe((res) => {
        console.log('the single data' + JSON.stringify(res));
        this.pricingSingleData = res;
        this.updateValue(this.pricingSingleData);
      });
  }

  updateValue(pricedata){
    console.log('price data in ' + JSON.stringify(pricedata));
    this.editprincingForm.patchValue({
      promotionName : pricedata.promotionName,
      promotionDescription: pricedata.promotionDescription,
      pricePerDay: pricedata.pricePerDay,
      pricePerMonth: pricedata.pricePerMonth,
    });
  }

  buildPricingForm(){
    this.editprincingForm = new FormGroup({
      promotionName : new FormControl(null, [Validators.required]),
      promotionDescription: new FormControl(null, [Validators.required]),
      pricePerDay: new FormControl(null, [Validators.required]),
      pricePerMonth: new FormControl(null, [Validators.required]),
    });

  }
  onSaveUpdating(){
    this.pricingData = this.editprincingForm.value;
    this.pricingservice.editPricing(this.singlePriceId, this.pricingData)
    .subscribe((response) => {
        console.log('this is the saving price ' + JSON.stringify(response));
        this.toaster.success('Pricing is Added  Edited data Successfully !');
        this.route.navigate(['pricing/homepricing']);
    });
  }
}