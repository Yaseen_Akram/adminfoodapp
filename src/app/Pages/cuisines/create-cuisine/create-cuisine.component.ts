import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CuisineServiceService } from '../cuisine-service.service';


@Component({
  selector: 'app-create-cuisine',
  templateUrl: './create-cuisine.component.html',
  styleUrls: ['./create-cuisine.component.css']
})
export class CreateCuisineComponent implements OnInit {
  createcuisineForm: FormGroup;
  cuisinedata: any;
  constructor(
    private route: Router,
    private toaster: ToastrService,
    private cuisineService: CuisineServiceService
    ) { }

  ngOnInit(): void {
  this.buildFormCuisine();
  }

  buildFormCuisine(){
  this.createcuisineForm = new FormGroup({
    cuisineName : new FormControl(null, [Validators.required])
    });
  }

  onSaveCuisine(){
    // console.log(this.createcuisineForm.value);
    this.cuisinedata = this.createcuisineForm.value;
    this.cuisineService.addcuisine(this.cuisinedata)
    .subscribe((response) => {
      console.log('this respose data' + JSON.stringify(response));
    });
    this.toaster.success('Cuisine Added SuccessFully !');
    this.route.navigate(['cuisines/homecuisine']);
  }
}
