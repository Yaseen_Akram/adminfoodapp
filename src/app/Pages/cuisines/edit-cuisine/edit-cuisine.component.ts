import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup} from '@angular/forms';
import { CuisineServiceService } from '../cuisine-service.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-cuisine',
  templateUrl: './edit-cuisine.component.html',
  styleUrls: ['./edit-cuisine.component.css']
})
export class EditCuisineComponent implements OnInit {
  cuisineSingleId: any;
  editcuisineForm: FormGroup;
  cuisineSingleData: any;
  cuisineData: any;
  editedcuisinedata: any;
  constructor(
    private cuisineservice: CuisineServiceService,
    private toaster: ToastrService,
    private route: Router,
    private activatedroute: ActivatedRoute  ) { }

  ngOnInit(): void {
    this.activatedroute.params
    .subscribe((paramsdata) => {
        console.log('params ID ' + JSON.stringify(paramsdata));
        this.cuisineSingleId = paramsdata.id;
    });
    this.buildForm();
    this.cuisineservice.getcuisinebyId(this.cuisineSingleId)
      .subscribe( (res) => {
          console.log('single profile data' + JSON.stringify(res));
          this.cuisineData = res;
          this.cuisineSingleData = this.cuisineData.cuisineName;
          this.updatevalue()
          console.log('the name is ' + this.cuisineSingleData);
      });
    // setTimeout(() => {
    //     this.updatevalue();
    //   }, 6000);
  }

  updatevalue(){
    console.log('control is comming into updatevalue'  );
    this.editcuisineForm.patchValue({
      cuisineName: this.cuisineSingleData
    });
  }
  buildForm(){
    this.editcuisineForm = new FormGroup({
      cuisineName : new FormControl(null, [Validators.required])
    });
  }
  onupdateCuisine(){
    this.editedcuisinedata = this.editcuisineForm.value;
    this.cuisineservice.addcuisine(this.editedcuisinedata)
    .subscribe((response) => {
      console.log('this respose data' + JSON.stringify(response));
    });
    this.toaster.success('Cuisine Edited SuccessFully !');
    this.route.navigate(['cuisines/homecuisine']);
  }
}
