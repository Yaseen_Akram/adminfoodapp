import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomecuisinesComponent } from './homecuisines.component';

describe('HomecuisinesComponent', () => {
  let component: HomecuisinesComponent;
  let fixture: ComponentFixture<HomecuisinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomecuisinesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomecuisinesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
