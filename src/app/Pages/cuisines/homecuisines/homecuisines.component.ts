import { Component, OnInit } from '@angular/core';
import {CuisineServiceService} from '../cuisine-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homecuisines',
  templateUrl: './homecuisines.component.html',
  styleUrls: ['./homecuisines.component.css']
})
export class HomecuisinesComponent implements OnInit {
  CuisineList: any;
  // title = 'Cusine datatables';
  // dtOptions: DataTables.Settings = {};
  constructor(
    private cuisineservice: CuisineServiceService,
    private route: Router) { }

  ngOnInit(): void {
    // this.dtOptions = {
    //   pagingType: 'full_numbers',
    //   pageLength: 5,
    //   processing: true
    // };
    this.getCuisineList();
  }

  getCuisineList(){
    this.cuisineservice.getcuisine()
      .subscribe( (respone) => {
        this.CuisineList = respone;
        console.log('The Resonse of the Cuisine ' + JSON.stringify(this.CuisineList));
      });
    }
    OnAddCuinsine(){
        this.route.navigate(['/cuisines/create']);
    }

    onEditCuisine(id){
       this.route.navigate(['/cuisines/edit/' + id ]);
    }

    onDeleteCuisine(id){
      console.log('this is the Delete ' + id );
    }
}
