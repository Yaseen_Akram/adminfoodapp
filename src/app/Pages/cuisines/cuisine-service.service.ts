import { Injectable } from '@angular/core';
import { ServiceService } from '../../service.service';

@Injectable({
  providedIn: 'root'
})
export class CuisineServiceService {

  constructor(
    private service: ServiceService
  ) { }

getcuisine(){
  return this.service.get('cuisines');
}

addcuisine(cuisinedata){
  return this.service.post('cuisines', cuisinedata);
}

getcuisinebyId(id){
  return this.service.getOne('cuisines', id);
 }

//required Api from the Backend Developer
// delete(id){
//   return this.service.delete('', id )
// }
}
