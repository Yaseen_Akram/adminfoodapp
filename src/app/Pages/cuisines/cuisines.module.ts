import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DataTablesModule} from 'angular-datatables';
import { ReactiveFormsModule } from '@angular/forms';

import { CuisinesRoutingModule } from './cuisines-routing.module';
import { CuisinesComponent } from './cuisines.component';
import { HomecuisinesComponent } from './homecuisines/homecuisines.component';
import { CreateCuisineComponent } from './create-cuisine/create-cuisine.component';
import { EditCuisineComponent } from './edit-cuisine/edit-cuisine.component';




@NgModule({
  declarations: [CuisinesComponent, HomecuisinesComponent, CreateCuisineComponent, EditCuisineComponent],
  imports: [
    CommonModule,
    CuisinesRoutingModule,
    DataTablesModule,
    ReactiveFormsModule

  ]
})
export class CuisinesModule { }
