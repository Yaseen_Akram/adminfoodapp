import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuisinesComponent } from './cuisines.component';
import { HomecuisinesComponent } from './homecuisines/homecuisines.component';
import { CreateCuisineComponent } from './create-cuisine/create-cuisine.component';
import { EditCuisineComponent } from './edit-cuisine/edit-cuisine.component';

const routes: Routes = [
  { path: '', component: CuisinesComponent },
  { path: 'homecuisine' , component: HomecuisinesComponent },
  { path: 'create', component: CreateCuisineComponent},
  { path: 'edit/:id', component: EditCuisineComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CuisinesRoutingModule { }
