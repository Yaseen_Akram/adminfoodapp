import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreatedeliveryboyComponent } from './createdeliveryboy.component';

describe('CreatedeliveryboyComponent', () => {
  let component: CreatedeliveryboyComponent;
  let fixture: ComponentFixture<CreatedeliveryboyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreatedeliveryboyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreatedeliveryboyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
