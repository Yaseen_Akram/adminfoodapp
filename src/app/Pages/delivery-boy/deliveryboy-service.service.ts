import { Injectable } from '@angular/core';
import {ServiceService} from '../../service.service';

@Injectable({
  providedIn: 'root'
})
export class DeliveryboyServiceService {

  constructor(
    private service: ServiceService
  ) { }

  getAlldeliveryboys(){
    return this.service.get('deliveryBoy');
  }
}
