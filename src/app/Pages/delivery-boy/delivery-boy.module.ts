import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryBoyRoutingModule } from './delivery-boy-routing.module';
import { DeliveryBoyComponent } from './delivery-boy.component';
import { HomedeliveryComponent } from './homedelivery/homedelivery.component';
import { CreatedeliveryboyComponent } from './createdeliveryboy/createdeliveryboy.component';
import { EditdeliveryboyComponent } from './editdeliveryboy/editdeliveryboy.component';


@NgModule({
  declarations: [DeliveryBoyComponent, HomedeliveryComponent, CreatedeliveryboyComponent, EditdeliveryboyComponent],
  imports: [
    CommonModule,
    DeliveryBoyRoutingModule
  ]
})
export class DeliveryBoyModule { }
