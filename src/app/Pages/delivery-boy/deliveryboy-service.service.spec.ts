import { TestBed } from '@angular/core/testing';

import { DeliveryboyServiceService } from './deliveryboy-service.service';

describe('DeliveryboyServiceService', () => {
  let service: DeliveryboyServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DeliveryboyServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
