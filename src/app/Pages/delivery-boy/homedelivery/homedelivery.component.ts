import { Component, OnInit } from '@angular/core';
import {DeliveryboyServiceService} from '../deliveryboy-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-homedelivery',
  templateUrl: './homedelivery.component.html',
  styleUrls: ['./homedelivery.component.css']
})
export class HomedeliveryComponent implements OnInit {
deliveryboyList: any;
  constructor(
    private deliveryservice: DeliveryboyServiceService,
    private route: Router) { }

  ngOnInit(): void {
    this.getDeliveryList();
  }
  getDeliveryList(){
    this.deliveryservice.getAlldeliveryboys()
      .subscribe((response) => {
          this.deliveryboyList = response;
          console.log('this is Delivery List are' + JSON.stringify( this.deliveryboyList));
      });
  }

  OnAddDeleveryboy(){
    this.route.navigate(['/deliveryboy/create']);
  }

  onEditDelivery(id){
    this.route.navigate(['/deliveryboy/edit/' + id]);
  }
}
