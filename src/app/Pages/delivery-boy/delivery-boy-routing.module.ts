import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeliveryBoyComponent } from './delivery-boy.component';
import { HomedeliveryComponent } from './homedelivery/homedelivery.component';
import { CreatedeliveryboyComponent } from './createdeliveryboy/createdeliveryboy.component';
import { EditdeliveryboyComponent } from './editdeliveryboy/editdeliveryboy.component';

const routes: Routes = [
   { path: '', component: DeliveryBoyComponent },
   { path: 'homedeliveryBoy', component: HomedeliveryComponent},
   { path: 'create', component: CreatedeliveryboyComponent},
   { path: 'edit/:id', component: EditdeliveryboyComponent}
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryBoyRoutingModule { }
