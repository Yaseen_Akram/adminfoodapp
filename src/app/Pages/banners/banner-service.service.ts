import { Injectable } from '@angular/core';
import { ServiceService } from '../../service.service';
// import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class BannerServiceService {

  constructor( private service: ServiceService ) { }

  getBanner(){
    return this.service.get('banner/getBanners');
  }

  approveBanner(id, data) { // Update Withdrawal Requests
    return this.service.patch('banner/approve', id, data);
  }

  rejectBanner(id, data) { // Update Withdrawal Requests
    return this.service.patch('banner/reject', id, data);
  }

  public deleteBanner(id) {
    return this.service.delete('banner/deleteBanner', id);
  }

}
