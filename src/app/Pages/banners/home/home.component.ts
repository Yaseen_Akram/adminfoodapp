import { Component, OnInit } from '@angular/core';
import { BannerServiceService } from '../banner-service.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  bannerList: any;
  constructor( private bannerservice: BannerServiceService,
               private toaster: ToastrService  ) { }

  ngOnInit(): void {
      this.getBannerList();
  }

  getBannerList(){
    this.bannerservice.getBanner()
    .subscribe( (response) => {
        this.bannerList = response;
        console.log(response);
    });
  }

  delete(id) {
    this.bannerservice.deleteBanner(id).subscribe((res: any) => {
      this.getBannerList();
      this.toaster.success('Banner has been removed!');
    });
  }
  rejectRequest(id) { // when admin reject the banner request
    const data = '';
    this.bannerservice.rejectBanner(id, data).subscribe((res: any) => {
      console.log('the RejectRequest ' + res);
      this.getBannerList();
      this.toaster.success('Banner has been reject!');
     });

  }

  confirmRequest(id) { // when admin reject the banner request
      const data = '';
      this.bannerservice.approveBanner(id, data).subscribe((res: any) => {
      console.log('the ApprovedRequest ' + res);
      this.getBannerList();
      this.toaster.success('Banner has been approve!');
     });

  }

}
