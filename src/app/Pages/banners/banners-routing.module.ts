import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BannersComponent } from './banners.component';
import {HomeComponent} from '../banners/home/home.component';

const routes: Routes = [
                        { path: '', component: BannersComponent },
                        { path: 'home', component: HomeComponent }
                        ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BannersRoutingModule { }
