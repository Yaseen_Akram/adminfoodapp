import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BannersRoutingModule } from './banners-routing.module';
import { BannersComponent } from './banners.component';
import { HomeComponent } from './home/home.component';



@NgModule({
  declarations: [BannersComponent, HomeComponent],
  imports: [
    CommonModule,
    BannersRoutingModule
  ]
})
export class BannersModule { }
