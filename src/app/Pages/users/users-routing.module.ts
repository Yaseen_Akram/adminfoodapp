import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UsersComponent } from './users.component';
import { HomeusersComponent } from './homeusers/homeusers.component';

const routes: Routes = [
  { path: '', component: UsersComponent },
  { path: 'homeusers', component: HomeusersComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
