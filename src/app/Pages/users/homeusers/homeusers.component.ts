import { Component, OnInit } from '@angular/core';
import { UsersServiceService } from '../users-service.service';

@Component({
  selector: 'app-homeusers',
  templateUrl: './homeusers.component.html',
  styleUrls: ['./homeusers.component.css']
})
export class HomeusersComponent implements OnInit {
  userList: any;
  constructor( private userservice: UsersServiceService) { }

  ngOnInit(): void {
    this.getallUserList();
  }
  getallUserList(){
    this.userservice.getAllusers()
      .subscribe((response) => {
        this.userList = response;
        console.log('this are user List' + JSON.stringify(this.userList));
      });
  }

  OnAddUser(){

  }

  onEditUser(id){

  }
}
