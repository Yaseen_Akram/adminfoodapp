import { Injectable } from '@angular/core';
import {ServiceService} from '../../service.service';

@Injectable({
  providedIn: 'root'
})
export class UsersServiceService {

  constructor(
    private service: ServiceService
  ) { }

  getAllusers(){
    return this.service.get('users');
  }
}
