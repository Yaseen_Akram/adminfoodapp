import { Component, OnInit } from '@angular/core';
import {TopcategoriesServiceService} from '../topcategories-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hometopcategories',
  templateUrl: './hometopcategories.component.html',
  styleUrls: ['./hometopcategories.component.css']
})
export class HometopcategoriesComponent implements OnInit {
TopCategoriesList: any;
TopCategoriesData: any;
  constructor(
    private topCategoriesService: TopcategoriesServiceService,
    private route: Router
  ) { }

  ngOnInit(): void {
    this.gettopcategoriesList();
  }

  gettopcategoriesList(){
    this.topCategoriesService.getallTopCategories()
      .subscribe( (response) => {
          this.TopCategoriesData = response;
          this.TopCategoriesList = this.TopCategoriesData.data;
          console.log('This are top Categories List' + JSON.stringify(this.TopCategoriesList));
      });
  }

  OnAddTopCategory(){
    this.route.navigate(['/topCategories/create']);
  }

  onEditCategory(id){
    this.route.navigate(['/topCategories/edit/' + id]);
  }

  onDeleteCategory(id){

  }
}
