import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HometopcategoriesComponent } from './hometopcategories.component';

describe('HometopcategoriesComponent', () => {
  let component: HometopcategoriesComponent;
  let fixture: ComponentFixture<HometopcategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HometopcategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HometopcategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
