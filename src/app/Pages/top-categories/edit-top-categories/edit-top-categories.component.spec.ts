import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTopCategoriesComponent } from './edit-top-categories.component';

describe('EditTopCategoriesComponent', () => {
  let component: EditTopCategoriesComponent;
  let fixture: ComponentFixture<EditTopCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTopCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTopCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
