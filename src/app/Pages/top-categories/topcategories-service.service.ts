import { Injectable } from '@angular/core';
import { ServiceService } from '../../service.service';

@Injectable({
  providedIn: 'root'
})
export class TopcategoriesServiceService {

  constructor(
    private service: ServiceService
  ) { }

  getallTopCategories(){
    return this.service.get('topCategories/list');
  }

  addTopCategories(data){
    console.log('controler comming into addTopCategories ' + JSON.stringify(data));
    return this.service.post('topCategories/create', data);
  }

  public uploadImage(file) {
    const formData = new FormData();
    formData.append('file', file);

    return this.service.UploadImage('uploads', formData);
  }
}
