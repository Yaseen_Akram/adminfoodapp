import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTopCategoriesComponent } from './create-top-categories.component';

describe('CreateTopCategoriesComponent', () => {
  let component: CreateTopCategoriesComponent;
  let fixture: ComponentFixture<CreateTopCategoriesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTopCategoriesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTopCategoriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
