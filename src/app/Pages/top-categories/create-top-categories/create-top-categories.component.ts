import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { TopcategoriesServiceService } from '../topcategories-service.service';

@Component({
  selector: 'app-create-top-categories',
  templateUrl: './create-top-categories.component.html',
  styleUrls: ['./create-top-categories.component.css']
})
export class CreateTopCategoriesComponent implements OnInit {
  topCategory: FormGroup;
  url: any;
  ImageUrlid: any;
  imageSelected: boolean;

    constructor(
      private topCategoriesService: TopcategoriesServiceService,
      private toaster: ToastrService,
      private route: Router
      ) { }
    ngOnInit(): void {
    this.buildForm();
    }

    buildForm(){
      this.topCategory = new FormGroup({
        categoryName : new FormControl(null, [Validators.required]),
        imageURL : new FormControl (null, [Validators.required])
      });
    }

    onSavetopCategory(){
      this.topCategory.setValue({
        categoryName: this.topCategory.value.categoryName,
        imageURL: this.ImageUrlid
      });
      // console.log(this.topCategory.value);
      this.topCategoriesService.addTopCategories(this.topCategory.value)
      .subscribe((response) => {
          console.log(JSON.stringify(response));
          this.route.navigate(['/topCategories/hometopCategories']);
          this.toaster.success('Top Categories is Added Successfull !!');
      });
      }

    readUrl(event) { // invoke when user select image to upload
      if (event.target.files && event.target.files[0]) {
          const reader = new FileReader();
          reader.onload = (event1: any) => {
              this.url = event1.target.result;
              this.imageSelected = true;
          };
          reader.readAsDataURL(event.target.files[0]);
          this.saveImageOnServer(event.target.files[0]);
      }
    }
    saveImageOnServer(image) {
      this.topCategoriesService.uploadImage(image)
          .subscribe((res: any) => {
            // console.log(res._id);
            this.ImageUrlid = res._id;
          });
      this.toaster.success('Image is uploaded Successfull !!');
  }
}