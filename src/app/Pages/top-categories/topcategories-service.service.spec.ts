import { TestBed } from '@angular/core/testing';

import { TopcategoriesServiceService } from './topcategories-service.service';

describe('TopcategoriesServiceService', () => {
  let service: TopcategoriesServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TopcategoriesServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
