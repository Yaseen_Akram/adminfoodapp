import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { TopCategoriesRoutingModule } from './top-categories-routing.module';
import { TopCategoriesComponent } from './top-categories.component';
import { HometopcategoriesComponent } from './hometopcategories/hometopcategories.component';
import { CreateTopCategoriesComponent } from './create-top-categories/create-top-categories.component';
import { EditTopCategoriesComponent } from './edit-top-categories/edit-top-categories.component';



@NgModule({
  declarations: [TopCategoriesComponent, HometopcategoriesComponent, CreateTopCategoriesComponent, EditTopCategoriesComponent],
  imports: [
    CommonModule,
    TopCategoriesRoutingModule,
    ReactiveFormsModule
  ]
})
export class TopCategoriesModule { }
