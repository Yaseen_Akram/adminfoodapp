import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TopCategoriesComponent } from './top-categories.component';
import { HometopcategoriesComponent } from './hometopcategories/hometopcategories.component';
import { CreateTopCategoriesComponent } from './create-top-categories/create-top-categories.component';
import { EditTopCategoriesComponent } from './edit-top-categories/edit-top-categories.component';

const routes: Routes = [
  { path: '', component: TopCategoriesComponent },
  { path: 'hometopCategories', component: HometopcategoriesComponent },
  { path: 'create', component: CreateTopCategoriesComponent },
  { path: 'edit/:id', component: EditTopCategoriesComponent }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TopCategoriesRoutingModule { }
