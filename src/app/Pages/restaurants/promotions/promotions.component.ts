import { Component, OnInit } from '@angular/core';
import {RestuarantServiceService} from '../restuarant-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-promotions',
  templateUrl: './promotions.component.html',
  styleUrls: ['./promotions.component.css']
})
export class PromotionsComponent implements OnInit {
  PromotionList: any;
  PromotionData: any;
  constructor(
    private restuarantserice: RestuarantServiceService,
    private toaster: ToastrService )  { }

  ngOnInit(): void {
    this.getpromotionList();
  }

  getpromotionList(){
    this.restuarantserice.getPromotionList()
    .subscribe( (response) => {
        this.PromotionData = response;
        this.PromotionList = this.PromotionData.data;
        console.log('The Promtions are' + JSON.stringify(this.PromotionList));
    });
  }

  rejectRequest(id) { // when admin reject the banner request
    const data = '';
    this.restuarantserice.rejectPromotion(id, data).subscribe((res: any) => {
      console.log('the RejectRequest ' + res);
      this.getpromotionList();
      this.toaster.success('Promotions has been reject!');
     });

  }

  confirmRequest(id) { // when admin reject the banner request
      const data = '';
      this.restuarantserice.approvePromotion(id, data).subscribe((res: any) => {
      console.log('the ApprovedRequest ' + res);
      this.getpromotionList();
      this.toaster.success('Promotions has been approve!');
     });

  }

}
