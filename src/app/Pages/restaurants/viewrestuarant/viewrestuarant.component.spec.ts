import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewrestuarantComponent } from './viewrestuarant.component';

describe('ViewrestuarantComponent', () => {
  let component: ViewrestuarantComponent;
  let fixture: ComponentFixture<ViewrestuarantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewrestuarantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewrestuarantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
