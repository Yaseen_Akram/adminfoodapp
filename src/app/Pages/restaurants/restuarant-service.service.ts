import { Injectable } from '@angular/core';
import {ServiceService} from '../../service.service';

@Injectable({
  providedIn: 'root'
})
export class RestuarantServiceService {

  constructor(private service: ServiceService) { }

  getRestuarants(){
    return this.service.get('restaurant');
  }
  approveRestuarant(id, data) { // Update Withdrawal Requests
    return this.service.patch('restaurant/approve', id, data);
  }

  rejectRestuarant(id, data) { // Update Withdrawal Requests
    return this.service.patch('restaurant/reject', id, data);
  }

  getPromotionList(){
     return this.service.get('restaurant/promotion/list');
  }


  approvePromotion(id, data) { // Update Withdrawal Requests
    return this.service.patch('restaurant/approve', id, data);
  }

  rejectPromotion(id, data) { // Update Withdrawal Requests
    return this.service.patch('restaurant/reject', id, data);
  }
}


