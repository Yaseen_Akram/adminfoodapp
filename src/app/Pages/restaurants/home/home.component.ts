import { Component, OnInit } from '@angular/core';
import { RestuarantServiceService } from '../restuarant-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  restuarantsList: any;
  constructor(
    private restuarantservice: RestuarantServiceService,
    private toaster: ToastrService  ) { }

  ngOnInit(): void {
    this.getRestuarantsList();
  }

  getRestuarantsList(){
    this.restuarantservice.getRestuarants()
    .subscribe((response) => {
        this.restuarantsList = response;
        console.log('the Resturants list is' + JSON.stringify(this.restuarantsList));
    });
  }

  rejectRequest(id) { // when admin reject the banner request
    const data = '';
    this.restuarantservice.rejectRestuarant(id, data).subscribe((res: any) => {
      console.log('the RejectRequest ' + res);
      this.getRestuarantsList();
      this.toaster.success('Resturants has been reject!');
     });

  }

  confirmRequest(id) { // when admin reject the banner request
      const data = '';
      this.restuarantservice.approveRestuarant(id, data).subscribe((res: any) => {
      console.log('the ApprovedRequest ' + res);
      this.getRestuarantsList();
      this.toaster.success('Resturants has been approve!');
     });

  }
}
