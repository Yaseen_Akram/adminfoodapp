import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConstantserviceService {
  public LOGIN_AUTH: string;  // contains login url
  public API_ENDPOINT: string;  // contains crud url
  public SOCKET_URL: string;
  public cloudinaryConfig: any;
  constructor() {
      // this.API_ENDPOINT = environment.API_ENDPOINT + 'api/';
     // this.LOGIN_AUTH = environment.API_ENDPOINT;
      this.API_ENDPOINT = 'http://93.115.20.44:7020/' + 'api/';
      this.LOGIN_AUTH = 'http://93.115.20.44:7020/';
      this.SOCKET_URL = environment.API_ENDPOINT;
      this.cloudinaryConfig = environment.cloudinaryConfig;
      console.log('the url in the costant service is' + this.LOGIN_AUTH);
      console.log('the url in the costant service is' + environment.API_ENDPOINT);
    }


}
