// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  //  API_ENDPOINT: "http://192.168.0.5:7020/",
  // API_ENDPOINT: 'https://lastminutejk.herokuapp.com/',
  // API_ENDPOINT: 'https://restaurant-sass.herokuapp.com/',
  // API_ENDPOINT: 'http://93.115.20.44:8008/',
    API_ENDPOINT: 'http://93.115.20.44:7020/',

    cloudinaryConfig: {
    cloudName: 'impnolife',
    uploadPreset: 'mspqunld'

  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
